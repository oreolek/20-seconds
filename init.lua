require 'para'
require 'dash'
require 'quotes'
require 'instead-modules/translate'
require 'instead-modules/emphasis'
require 'instead-modules/rndstr'
require 'proxymenu'
require "kbd"
require "hideinv"

game.forcedsc = true;

-- grabbing the Escape key
function init()
  hook_keys('escape');
end

pl._time = nil;

game.kbd = function(s, down, key)
  p [[А тут мы поймали Escape и переходим на выбор уровня. Но пока что выбора уровня нет.
  И сейвов не будет, будет prefs. А рестарт игры по кнопке в инвентаре.
  
  Кстати, почему dsc у сцены не вызывается сразу?]];
end 

-- get rid of idiotic combinations in the verb menu
stead.proxy_fill_objs = function(s, w, act, use_mode, used_act, useit_act)
  local ii,i,o
  local rc = false
  for i,o,ii in opairs(w) do
    o = stead.ref(o);
    if isObject(o) and not isDisabled(o) and o ~= s and not isPhrase(o) and o[act] ~= false
      and not o.proxy_type and not isStatus(o) then

      s.obj:add(stead.obj_proxy(o, act, use_mode, used_act, useit_act));
      if not isRoom(o) then
	stead.proxy_fill_objs(s, o.obj, act, use_mode, used_act, useit_act);
      end
      rc = true
    end
  end
  return rc
end

-- inventory object with no double click
pocketobj = function(object)
  local v = { };
  v.action_type = true;
  v.nam = object.nam;
  v.tak = object.tak;
  v.dsc = object.dsc;
  v.gen = function() end;
  v.inv = object.inv;
  return menu(v);
end

restart_call = pocketobj {
  nam = txtb(_('Заново')),
  inv = function()
    stead.restart()
  end,
}
place(restart_call, me());

clock = stat {
  nam = 'часы',
  disp = function()
    if pl._time ~= nil then
      p(pl._time);
      pn(_('секунд'));
    end
  end,
}
place(clock, me());

-- меню глаголов
game.talk = _('Я не уверен, что расслышал ответ.');
game.examine = _('Не могу выразиться точнее.');
game.pickup = _('Я трачу несколько минут на поиск удобного способа взять это с собой. Не выходит.');
game.hit = _('Вряд ли выдержит даже одного удара.');
game.smell = _('Нет запаха.');
game.lick = _('Нет вкуса.');
game.touch = _('Нет реакции.');
game.push = _('Не двигается.');
game.pull = _('Не поддаётся.');
talk = obj_menu(_('ГОВОРИТЬ'), 'talk', true, true)
examine = obj_menu(_('ОСМОТРЕТЬ'), 'act', true, true)
pickup = obj_menu(_('ВЗЯТЬ'), 'pickup', true);
hit = obj_menu(_('УДАРИТЬ'), 'hit', true);
smell = obj_menu(_('ОБНЮХАТЬ'), 'smell', true);
lick = obj_menu(_('ЛИЗНУТЬ'), 'lick', true);
touch = obj_menu(_('ТРОНУТЬ'), 'touch', true);
push = obj_menu(_('ТОЛКНУТЬ'), 'push', true);
pull = obj_menu(_('ТЯНУТЬ'), 'pull', true);
place(examine, me())
place(smell, me())
place(lick, me())
place(touch, me())
place(pickup, me())
place(talk, me())
place(hit, me())
place(push, me())
place(pull, me())
self = obj {
  nam = _('себя'),
  dsc = '',
  act = _('Прекрасен и здоров.'),
  take = _('Уже сделано.'),
  hit = _('Я не мазохист.'),
}
