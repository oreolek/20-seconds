-- $Name: 20 секунд$
-- $Name(en): 20 seconds$

instead_version "1.8.0"
require 'init'

main = room{
  nam = 'Выбор уровня',
  dsc = function()
    walk('room1');
  end,
  exit = function()
    pl._time = 20;
    lifeon('room1');
    return _([[У вас 20 секунд на то, чтобы спасти Землю.]]);
  end
};

gameover = room {
  nam = _('Игра окончена'),
  hideinv = true,
  enter = function()
    pl._time = nil
  end,
  dsc = _('Вы проиграли.')
}

room1 = room {
  nam = _('Пляж на речке Грязная'),
  dsc = _([[Светит жаркое солнце. Вы находитесь на солнечном пляже.]]),
  obj = {
    'sand','alien', 'self'
  },
  life = function()
    stead.rndseed(os.time(os.date("*t")))
    pl._time = pl._time - 1;
  end
}

alien = obj {
  nam = 'инопланетянин',
  dsc = _('В кустах притаился {инопланетянин.}'),
  act = _('Серый гуманоид с двумя парами глаз и длинным хоботом вместо рта.'),
  talk = rndstr({
    _('Он издаёт серию чпоков.'),
    _('Гуманоид не замечает вас.'),
    _('Инопланетянин мяукает.'),
    _('Инопланетянин молчит.'),
    _('Вы не знаете, что ему сказать.'),
  }),
  hit = function()
    gameover.dsc = _('Инопланетянин уничтожает вас взглядом.');
    lifeoff('room1');
    walk('gameover');
  end,
}

sand = obj {
  nam = 'песок',
  dsc = [[Вокруг вас чистейший {песок.}]],
  act = 'Жёлтый песок образцового пляжа. По качеству стоит всего на три четверти ниже песков пустыни Сахары.',
  talk = false,
  hit = false,
  pull = false,
  push = false,
  pickup = 'Я смотрю на банку с вареньем. Оно, конечно, вкусное, да у меня ложки нет. Не хочу есть пальцами.'
}
